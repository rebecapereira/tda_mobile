/**
 * PostsController
 *
 * @description :: Server-side logic for managing Posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  LoadNewPosts: function(req, res){

    var query = 'Select posts.post as post, posts.post_date as post_date,' +
    'members.members_display_name as username, members.posts as userposts,' +
    'avatars.avatar_location as avatar, ' +
    'groups.prefix as prefix, groups.g_title as g_title, groups.suffix as suffix, ' +
    'topics.title as title, forums.name as forumname ' +
    'FROM ibf_posts posts, ibf_members members, ibf_groups groups, ibf_topics topics, ibf_forums forums , ibf_member_extra as avatars ' +
    'WHERE posts.author_id = members.member_id ' +
    'and members.member_group_id = groups.g_id ' +
    'AND posts.topic_id = topics.tid ' +
    'AND topics.forum_id = forums.id ' +
    'AND members.member_id = avatars.id ' +
    'order by post_date desc ' +
    'limit 10';
    console.log(query);
    Members.query(query, function(err, posts){

      if(err){
        return res.status(400).json({error: err.message});
      }

      /*return res.view('newPosts'{
        posts:posts
      });*/

      console.log(posts);
      //return res.view( 'newposts', {posts: posts});
      return res.status(200).json({ posts: posts });

    });
  }

};

